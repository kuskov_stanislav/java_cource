package ru.systemres.kuskov;

import clojure.lang.IFn;
import com.thoughtworks.xstream.mapper.Mapper;

public class Main {

    public static void main(String[] args) {
//	// write your code here
//        class Car{
//            String color;
//            int count_of_wheels;
//            int power;
//            String type;
//            // Это метод
//            void run(int some_value){
//
//                System.out.println("speed: "+ some_value + ", drive car");
//            }
//            //Это конструктор
//            Car(){
//                color="green";
//                count_of_wheels=4;
//
//
//            }
//        }
//        Car LadaVesta = new Car();
//        LadaVesta.color="red";
//        LadaVesta.count_of_wheels=4;
//        LadaVesta.power=150;
//        LadaVesta.type="Gasoline";
////        System.out.println("Its my car "+LadaVesta.color+"with count of wheels: "+ LadaVesta.count_of_wheels);
//        System.out.printf("Car color: %s, Car count of wheels: %s \n", LadaVesta.color, LadaVesta.count_of_wheels);
//        LadaVesta.run(22);
//
//        //Цвет задан в конструкторе
//        Car LadaVesta2=new Car();
//        System.out.println(LadaVesta2.color);
//        System.out.println(LadaVesta2.power);
//
//        System.out.println("");
//
//        class Car{
//            String color;
//            int count_of_wheels;
//            int power;
//            String type;
//
//            Car(String some_color,int some_count_of_wheel,int some_power,String some_type){
//                this.color=some_color;
//                this.count_of_wheels=some_count_of_wheel;
//                this.power=some_power;
//                this.type=some_type;
//            }
//        }
//
//        Car LadaVesta=new Car("black",4,110,"Gasoline");
//        Car Porshe911=new Car("yellow",4,515,"Gasoline");
//
//        class Bycicle{
//            String color;
//            float count_of_wheels;
//            String material;
//            String type;
//
//            Bycicle(String color,float count_of_wheels,String material,String type ){
//                this.color=color;
//                this.count_of_wheels=count_of_wheels;
//                this.material=material;
//                this.type=type;
//            }
//        }
//        Bycicle First = new Bycicle("red",115.5f,"metal","road");
//        Bycicle Second = new Bycicle("green",3,"metal","fun");
//
//        System.out.printf("Color: %s \n", First.color);


//    //arrays
//        int my_array[]=new int[4];
//        my_array[0]=1;
//        my_array[1]=2;
//        my_array[2]=5;
//        my_array[3]=20;
//        for (int i=0;i<4;i++){
//            System.out.println(my_array[i]);
//        }

        //string arrays
//        String string_array[]={
//                "Понедельник",
//                "Вторник",
//                "Среда",
//                "Четверг",
//                "Пятница",
//                "Суббота",
//                "Воскресенье"
//        };

//        String mounts[] = {"jan", "feb", "marc", "april", "may", "june", "jule", "agust", "sept", "oct", "nov", "dec"};
//        int count_of_day[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
//        String seasons[] = {"winter", "winter", "spring", "spring", "spring", "summer", "summer", "summer", "atumn", "atumn", "atumn", "winter"};
//        int i = 0;
//        for (String x : mounts) {
//            System.out.printf("mount %s its season %s with %s \n", mounts[i], seasons[i], count_of_day[i]);
//            i++;
//        }
//        int a=10;
//        int b=0;
//        int c = 0;
//
//        try {
//            c=a/b;
//            System.out.println(c);
//        } catch (Exception ex){
//            System.out.println(ex.getMessage());
//        } finally {
//            System.out.println("Этот блок сработает в любом случае");
//        }
//
//        int mount=10;
//        int day=0;
//        String season;
//        switch (mount){
//            case 1: case 2: //можно, но не нужно
//                season="winter";
//                break;
//            case 3:
//                season="spring";
//                break;
//            case 6:
//                season="sommer";
//                break;
//            case 10:
//                season = "atumn";
//                day=25;
//                break;
//            default:
//                season="noname";
//
//        }
//        System.out.printf("октябрь это %s \n",season);

//        double a=10;
//        double b=11;
////        double c=(a>b)?a+b:Math.sqrt(b);
//
//        String c;
//        if (a!=10){
//            c="privet";
//        } else {
//            c = "poka";
//        }
////Переключатель, тернарная форма записи
//
////        if (a>b){
//////            System.out.println("a>b");
////            c=55;
////        } else {
//////            System.out.println("a<b");
////            c=100;
////        }
//        System.out.println(c);

//        boolean login=true;
//        boolean password=true;
//
//        if (login&&password){
//            System.out.println("welcome");
//        } else {
//            System.out.println("acces denied");
//        }

//        boolean chiken = true;
//        boolean fish = false;
//
//        if (chiken&fish){
//            System.out.println("chiken or fish");
//        } else if(chiken&!fish){
//            System.out.println("give fish");
//        } else if(!chiken&fish){
//            System.out.println("give chiken");
//        }
//        else {
//            System.out.println("go to kitchen");
//        }

//        boolean dtp=false;
//        boolean straf=false;
//
//        if (dtp || straf) {
//            System.out.println("includ pov coef");
//        }else {
//            System.out.println("usual money");
//        }

//        int a=4;
//        int b=8;
//        int c=a|b;
//        System.out.println(c);
//
//        int x=25;
//
//        if (x<50 && x<0){
//            System.out.println("yes");
//        }
//        if (x<50 & x<0){
//            System.out.println("no");
//        }

//        int a = 2147483647;
//        long b = a;
//
//        long c = 2147483647;
//        int d = (int) c;
//        System.out.println(d);

//        String a="12";
//        try{
//            int b= Integer.parseInt(a)/0;
//            System.out.println(b);
//        } catch (NumberFormatException ex){
//            System.out.printf("Input value please ");
//        } catch (ArithmeticException ex){
//            System.out.println("devide by zero");
//        }

//    int a=0;
//    while (a<10){
//        System.out.println(a);
//        a++;
//
//    }
//    do{
//        System.out.println(a);
//        a++;
//    }while (a<0);

//        for (int i=0;i<100;i++){
//            if (i==5){
//                System.out.println("i = "+i);
//                break;
//            }
//            System.out.println(i);
//        }
//        class Dog{
//            String color;
//            int size;
//            int weight;
//            void Gav(){
//                System.out.println("gavgavgav");
//            }
//            Dog(String color, int size,int weight){
//                this.color=color;
//                this.size=size;
//                this.weight=weight;
//            }
//            //перегруженный конструктор
//            Dog(String color, int size){
//                this.color=color;
//                this.size=size;
//            }
//            Dog(String color){
//                this.color=color;
//
//            }
//            Dog(int size){
//                this.size=size;
//
//            }
//            Dog(){
//
//            }
//        }
//        String x="red";
//        Dog Sherik=new Dog("white",30,20);
//        Dog Bonik=new Dog(30);
//
//        System.out.println(Sherik.color+" "+Sherik.size+" "+Sherik.weight);
//
//        int myMassiv[]={5,10,15,20,30};
//        int sum=0;
//        for (int i:myMassiv) {
//            System.out.println(i);
//        }

//        String myMassiv[] = {"Ivanov","Petrov","Rozhkov","Sokolov"};
//        String second_name="Petrov";
//        for (String i:myMassiv){
//            if (i==second_name){
//                System.out.println("secondName finded");
//                break;
//            }
//
//        }
        // 1 способ создать mystring
//        String myString = new String("Привет");
//        String myString2=new String(myString);
//        String myString3="Hello2";
//        System.out.println(myString2);
//        String myString4=new String();
//
//        char a= myString.charAt(0);
//        System.out.println(a);
//        if (myString.compareToIgnoreCase("привет")==0){
//            System.out.println("Ravni");
//        }
//        if (myString.compareTo("Привет")==0){
//            System.out.println("Ravni");
//        }
        //Перегруженный метод
        class Dog {
            void Gav(){
                System.out.println("Gev vithout param");
            }
            void Gav(int a){
                System.out.println("Gev vith 1 param");
            }
            void Gav(int a,int b){
                System.out.println("Gev vith 2 param");
            }
            void Gav(int a,int b, String c){
                System.out.println("Gev vith 3 param");
            }
        }
        Dog myDog=new Dog();
        myDog.Gav(1,2,"Hello");
    }

}
